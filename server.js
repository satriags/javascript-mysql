const express = require('express');
const mysql = require('mysql');
const BodyParser = require('body-parser');
const app = express();

app.use(BodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs');
app.set('views', 'views');

const db = mysql.createConnection({
    host: 'localhost',
    database: 'school',
    user: 'root',
    password: '',
});

db.connect((err) => {
    if(err) throw err;
    console.log('database connected');



    // ROUTE DATA UNTUK GET DATA
    app.get('/', (req, res) => {


        // MENAMPILKAN SEMUA DATA SERTA MEMBUAT WEB MEMPERBARUI DATA
        const sql = "SELECT * FROM user";
        db.query(sql, (err, result) => {

            // MEMPARSE AGAR TIDAK MENGHASILKAN TYPE DATA BER OBJECT
            const users = JSON.parse(JSON.stringify(result));
            res.render('index', { users: users, title: 'DAFTAR MURID' });
        });
        // res.send(users);
    });

    app.get('/hapus/:id', (req, res) => {

        const insertSql = `DELETE FROM user WHERE id='${req.params.id}'`;
        // console.log(insertSql);
        db.query(insertSql, (err, result) => {
            if(err) throw err;
            res.redirect("/");
        });
    });

    app.get('/profile', (req, res) => {
        res.render('profilenya', { title: 'PROFILE DATA' });

    });

    // UNTUK INSERT DATA
    app.post('/tambah', (req, res) => {
        const insertSql = `INSERT INTO user (nama,kelas) VALUES ('${req.body.nama}','${req.body.kelas}')`;
        db.query(insertSql, (err, result) => {
            if(err) throw err;
            res.redirect("/");
        });
    });





});

// CREATE SERVER PRIBADI
app.listen(8080, () => {
    console.log('server ready');
})

